using FeedbackCore.Common;
using FeedbackCore.Models;
using FeedbackCore.Repositories;
using FeedbackCore.Services;
using MongoDB.Bson;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FeedbackCoreUnitTestProject
{
    public class FeedbackServiceUnitTest
    {
        private Mock<IFeedbackRepository> _mockFeedbackRepository;
        private FeedbackService _feedbackService;
        private IEnumerable<Feedback> feedbacks = GetAllFeedbacks();
        private Feedback feedback = GetFeedbackTest();

        [SetUp]
        public void Setup()
        {
            _mockFeedbackRepository = new Mock<IFeedbackRepository>();
            _feedbackService = new FeedbackService(_mockFeedbackRepository.Object);
        }

        [Test]
        public async Task PopulateDatabase_Verify()
        {
            //Act
            await _feedbackService.Create(feedback).ConfigureAwait(false);

            //Assert
            _mockFeedbackRepository.Verify(f => f.Create(It.IsAny<Feedback>()));
        }

        [Test]
        public async Task GetAllFeedbacksFromDatabase()
        {
            //Arrange
            _mockFeedbackRepository.Setup(f => f.Get()).ReturnsAsync(feedbacks);

            //Act
            var actual = await _feedbackService.Get().ConfigureAwait(false);

            //Assert
            Assert.AreEqual(2, actual.Count() );
        }

        [Test]
        public async Task GetByUsername_ReturnFeedback_IfUsernameExists()
        {
            //Arrange
            string username = RandomGenerator.GetString();
            _mockFeedbackRepository.Setup(f => f.GetByUserName(username)).ReturnsAsync(feedbacks);

            //Act
            var actual = await _feedbackService.GetByUserName(username).ConfigureAwait(false);

            //Assert
            Assert.AreEqual(feedbacks.Count(), actual.Count());
        }

        [Test]
        public async Task GetByUsername_ReturnFalse_IfUsernameDoesNotExists()
        {
            //Arrange & Act
            string username = RandomGenerator.GetString();
            var actual = await _feedbackService.GetByUserName(username).ConfigureAwait(false);

            //Assert
            Assert.IsFalse(actual.Any());
        }

        [Test]
        public async Task GetById_ReturnFeedback_IfIdExists()
        {
            //Arrange
            var id = RandomGenerator.GetString();
            _mockFeedbackRepository.Setup(f => f.GetById(id)).ReturnsAsync(feedback);

            //Act
            var actual = await _feedbackService.GetById(id).ConfigureAwait(false);

            //Assert
            Assert.AreEqual(feedback.Id, actual.Id);
        }

        [Test]
        public async Task GetById_ReturnNull_IfIdDoesNotExists()
        {
            //Arrange
            var id = RandomGenerator.GetString();

            //Act
            var actual = await _feedbackService.GetById(id).ConfigureAwait(false);

            //Assert
            Assert.IsNull(actual);
        }

        [Test]
        public async Task RemoveFeedback_Verify()
        {
            //Arrange
            var id = RandomGenerator.GetString();

            //Act
            await _feedbackService.Remove(id).ConfigureAwait(false);

            //Assert
            _mockFeedbackRepository.Verify(f => f.Remove(id));
        }

        [Test]
        public async Task UpdateFeedback_Verify()
        {
            //Arrange
            var id = RandomGenerator.GetString();

            //Act
            await _feedbackService.Update(id, feedback).ConfigureAwait(false);

            //Assert
            _mockFeedbackRepository.Verify(f => f.Update(id, feedback));
        }

        [Test]
        public async Task Search_ReturnFeedbacks_IfStringExists()
        {
            //Arrange
            string search = RandomGenerator.GetString();
            var expected = GetListTest();
            _mockFeedbackRepository.Setup(f => f.Search(search)).ReturnsAsync(expected);

            //Act
            var actual = await _feedbackService.Search(search).ConfigureAwait(false);

            //Assert
            Assert.AreEqual(expected.Count(), actual.Count());
        }

        [Test]
        public async Task Search_ReturnFalse_IfStringDoesNotExist()
        {
            //Arrange & Act
            string search = RandomGenerator.GetString();
            var actual = await _feedbackService.Search(search).ConfigureAwait(false);

            //Assert
            Assert.IsFalse(actual.Any());
        }

        public static Feedback GetFeedbackTest()
        {
            return new Feedback
            {
                DateTime = DateTime.Now.ToString(),
                Description = RandomGenerator.GetString(),
                Name = RandomGenerator.GetString(),
                UserId = RandomGenerator.Integer(1, 3)
            };
        }

        public static IEnumerable<Feedback> GetAllFeedbacks()
        {
            var feedback = new Feedback
            {
                DateTime = DateTime.Now.ToString(),
                Description = RandomGenerator.GetString(),
                Name = RandomGenerator.GetString(),
                Id = ObjectId.Parse(RandomGenerator.GetRandomHexNumber(24).ToString()),
                UserId = RandomGenerator.Integer(1, 3)
            };

            var feedback1 = new Feedback
            {
                DateTime = DateTime.Now.ToString(),
                Description = RandomGenerator.GetString(),
                Name = RandomGenerator.GetString(),
                Id = ObjectId.Parse(RandomGenerator.GetRandomHexNumber(24).ToString()),
                UserId = RandomGenerator.Integer(1, 3)
            };

            var feedbacks = new List<Feedback>
            {
                feedback, feedback1
            };

            return feedbacks;
        }

        public static IEnumerable<string> GetListTest()
        {
            var s = new List<string>
            {
                RandomGenerator.GetString(),
                RandomGenerator.GetString()
            };

            return s;
        }
    }
}