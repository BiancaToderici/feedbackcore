﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FeedbackCoreUnitTestProject
{
    public static class RandomGenerator
    {
        private static Random random = new Random();

        public static string GetString()
        {
            const int length = 10;
            const string chars = "abcdefghijklmnopqrstuvwxyz";
            var builder = new StringBuilder();
            for (var i = 0; i < length; i++)
            {
                var c = chars[random.Next(0, chars.Length)];
                builder.Append(c);
            }
            return builder.ToString();
        }

        public static int Integer(int min, int max)
        {
            var number = min > max ? random.Next(max, min) : random.Next(min, max);
            return number;
        }

        public static string GetRandomHexNumber(int digits)
        {
            byte[] buffer = new byte[digits / 2];
            random.NextBytes(buffer);
            string result = String.Concat(buffer.Select(x => x.ToString("X2")).ToArray());
            if (digits % 2 == 0)
                return result;
            return result + random.Next(16).ToString("X");
        }
    }
}
