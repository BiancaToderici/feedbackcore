﻿using BookingCore.Repository;
using FeedbackCore.Common;
using FeedbackCore.Repositories;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace FeedbackCoreUnitTestProject
{
    public class FeedbackRepositoryUnitTest 
    {
        private FeedbackRepository _feedbackRepository;
        private FeedbackContext _context;

        [SetUp]
        public void SetUp()
        {
            Settings appSettings = new Settings { ConnectionString = "mongodb://localhost:27017" , Database = "FeedbackDbTests" };
            IOptions<Settings> options = Options.Create(appSettings);
            _context = new FeedbackContext(options);
            _feedbackRepository = new FeedbackRepository(options);
        }

        [TearDown]
        public void TestCleanup()
        {
            _context.DropCollection();
        }

        [Test]
        public async Task Create_PopulateDatabase()
        {
            //Arrange
            var expected = GenerateFeedback();

            //Act
            await _feedbackRepository.Create(expected).ConfigureAwait(false);

            var actual = _context.Feedbacks.Find(f => f.Name == expected.Name).SingleOrDefault();

            //Assert
            Assert.AreEqual(expected.Id, actual.Id);
        }

        [Test]
        public async Task GetFeedbacksFromDb_ReturnTrue_IfFeedbacksExists()
        {
            //Arrange
            var feedback = await CreateTestFeedback().ConfigureAwait(false);

            //Act
            var actual = await _feedbackRepository.Get().ConfigureAwait(false);

            //Assert
             Assert.IsTrue(actual.Any());
        }

        [Test]
        public async Task GetByUsername_ReturnTrue_IfNameExists()
        {
            //Arrange
            var expected = await CreateTestFeedback().ConfigureAwait(false);

            //Act
            var feedbacks = await _feedbackRepository.GetByUserName(expected.Name).ConfigureAwait(false);
            
            var actual = _context.Feedbacks.Find(f => f.Id == expected.Id).SingleOrDefault();

            //Assert
            Assert.AreEqual(expected.Id, actual.Id);
        }

        [Test]
        public async Task GetByUsername_ReturnFalse_IfNameExists()
        {
            //Arrange
            var expected = GenerateFeedback();

            //Act
            var actual = await _feedbackRepository.GetByUserName(expected.Name).ConfigureAwait(false);

            //Assert
            Assert.IsFalse(actual.Any());
        }

        [Test]
        public async Task GetBySearch_ReturnTrue_IfStringExists()
        {
            //Arrange
            var expected = await CreateTestFeedback().ConfigureAwait(false);

            //Act
            var actual = await _feedbackRepository.Search(expected.Name).ConfigureAwait(false);

            //Assert
            Assert.IsTrue(actual.Any());
        }

        [Test]
        public async Task GetBySearch_ReturnFalse_IfStringDoesNotExists()
        {
            //Arrange
            var search = RandomGenerator.GetString();

            //Act
            var actual = await _feedbackRepository.Search(search).ConfigureAwait(false);

            //Assert
            Assert.IsFalse(actual.Any());
        }

        [Test]
        public async Task GetById_ReturnFeedback_IfIdExists()
        {
            //Arrange
            var expected = await CreateTestFeedback().ConfigureAwait(false);

            //Act
            var actual = await _feedbackRepository.GetById(expected.Id.ToString()).ConfigureAwait(false);

            //Assert
            Assert.AreEqual(expected.Name, actual.Name);
        }

        [Test]
        public async Task GetById_ReturnNull_IfIdDoesNotExists()
        {
            //Arrange
            var expected = GenerateFeedback();

            //Act
            var actual = await _feedbackRepository.GetById(expected.Id.ToString()).ConfigureAwait(false);

            //Assert
            Assert.IsNull(actual);
        }

        [Test]
        public async Task Remove()
        {
            //Arrange
            var expected = await CreateTestFeedback().ConfigureAwait(false);

            //Act
            await _feedbackRepository.Remove(expected.Id.ToString()).ConfigureAwait(false);

            var actual = _context.Feedbacks.Find(f => f.Id == expected.Id).SingleOrDefault();

            //Assert
            Assert.IsNull(actual);
        }

        [Test]
        public async Task Update()
        {
            //Arrange
            var feedback = await CreateTestFeedback().ConfigureAwait(false);
            Feedback expected = new Feedback
            {
                Id = feedback.Id,
                DateTime = DateTime.Now.ToString(),
                Description = RandomGenerator.GetString(),
                Name = RandomGenerator.GetString(),
                UserId = RandomGenerator.Integer(1, 3)
            };

            //Act
            await _feedbackRepository.Update(expected.Id.ToString(), expected).ConfigureAwait(false);

            var actual = _context.Feedbacks.Find(f => f.Id == expected.Id).SingleOrDefault();

            //Assert
            Assert.AreEqual(expected.Description, actual.Description);
        }

        private async Task<Feedback> CreateTestFeedback()
        {
            var feedback = GenerateFeedback();
            await _feedbackRepository.Create(feedback).ConfigureAwait(false);
            return feedback;
        }

        private static Feedback GenerateFeedback()
        {
            return new Feedback
            {
                DateTime = DateTime.Now.ToString(),
                Description = RandomGenerator.GetString(),
                Name = RandomGenerator.GetString(),
                UserId = RandomGenerator.Integer(1, 3)
            };
        }
    }
}
