﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FeedbackCore.Common
{
    public class Settings
    {
        public string ConnectionString;
        public string Database;
    }
}
