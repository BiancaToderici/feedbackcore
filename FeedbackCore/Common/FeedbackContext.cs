﻿using FeedbackCore.Common;
using FeedbackCore.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace BookingCore.Repository
{
    public class FeedbackContext
    {
        private IMongoDatabase _database = null;

        public FeedbackContext(IOptions<Settings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
            {
                _database = client.GetDatabase(settings.Value.Database);
            }

        }

        public IMongoCollection<Feedback> Feedbacks
        {
            get
            {
                return _database.GetCollection<Feedback>("Feedbacks");
            }
        }

        public void DropCollection()
        {
            _database.DropCollection("Feedbacks");
        }
    }
}