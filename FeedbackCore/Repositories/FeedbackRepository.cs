﻿using BookingCore.Repository;
using FeedbackCore.Common;
using FeedbackCore.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FeedbackCore.Repositories
{
    public class FeedbackRepository : IFeedbackRepository
    {
        private readonly FeedbackContext _context;
        public FeedbackRepository(IOptions<Settings> settings)
        {
            _context = new FeedbackContext(settings);
        }

        public async Task Create(Feedback feedback)
        {
            await _context.Feedbacks.InsertOneAsync(feedback).ConfigureAwait(false);
        }

        public async Task<IEnumerable<Feedback>> Get()
        {
            var feedbacks = await _context.Feedbacks.Find(_ => true).ToListAsync().ConfigureAwait(false);
            return feedbacks;
        }

        public async Task<IEnumerable<Feedback>> GetByUserName(string userName)
        {
            IQueryable<Feedback> query = _context.Feedbacks.AsQueryable();
            query = query.Where(u => u.Name.ToLower().Contains(userName));
            var feedbacks = await ((IMongoQueryable<Feedback>)query).ToListAsync().ConfigureAwait(false);
            return feedbacks;
        }

        public async Task<IEnumerable<string>> Search(string search)
        {
            IQueryable<string> query = _context.Feedbacks.AsQueryable().Select(u =>u.Name);
            query = query.Where(u => u.ToLower().Contains(search));
            var feedbacks = await ((IMongoQueryable<string>)query).Distinct().ToListAsync().ConfigureAwait(false);
            return feedbacks;
        }

        public async Task<Feedback> GetById(string id)
        {
            var internalId = ObjectId.Parse(id);
            var feedback = await _context.Feedbacks.Find(f => f.Id == internalId).SingleOrDefaultAsync().ConfigureAwait(false);
            return feedback;
        }

        public async Task Remove(string id)
        {
            var internalId = ObjectId.Parse(id);
            await _context.Feedbacks.DeleteOneAsync(f => f.Id == internalId).ConfigureAwait(false);
        }

        public async Task Update(string id, Feedback feedback)
        {
            var internalId = ObjectId.Parse(id);
            Feedback feedbackEdited = new Feedback
            {
                Id = internalId,
                Description = feedback.Description,
                DateTime = DateTime.Now.ToString(),
                Name = feedback.Name,
                UserId = feedback.UserId
            };
            await _context.Feedbacks.ReplaceOneAsync(f => f.Id == internalId, feedbackEdited).ConfigureAwait(false);
        }
    }
}
