﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FeedbackCore.Common;
using FeedbackCore.Models;
using FeedbackCore.Services;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using Newtonsoft.Json;

namespace FeedbackCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FeedbackController : ControllerBase
    {
        private IFeedbackService _feedbackService;
        public FeedbackController(IFeedbackService feedbackService)
        {
            _feedbackService = feedbackService;
        }
     
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var feedbacks = await _feedbackService.Get().ConfigureAwait(false);
            return Ok(feedbacks);
        }

        [Route("[action]/{userName}")]
        [HttpGet]
        public async Task<IActionResult> GetByUsername(string userName)
        {
            var feedbacks = await _feedbackService.GetByUserName(userName).ConfigureAwait(false);
            return Ok(feedbacks);
        }

        [Route("[action]/{search}")]
        [HttpGet]
        public async Task<IActionResult> Search(string search)
        {
            var names = await _feedbackService.Search(search).ConfigureAwait(false);
            return Ok(names);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
           var feedback = await _feedbackService.GetById(id).ConfigureAwait(false);
            return Ok(feedback);
        }

        // POST api/values
        [HttpPost]
        public async Task Add([FromBody] string feedbackModel)
        {
            var feedBack = JsonConvert.DeserializeObject<FeedbackModel>(feedbackModel);
            var feedback = new Feedback()
            {
                Description = feedBack.Description,
                UserId = feedBack.UserId,
                Name = feedBack.Name,
                DateTime = DateTime.Now.ToString()
            };
            await _feedbackService.Create(feedback).ConfigureAwait(false);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task Put(string id, [FromBody] string feedback)
        {
            var feedbackModel = JsonConvert.DeserializeObject<FeedbackModel>(feedback);
            var feedbackEdited = new Feedback()
            {
                Id = ObjectId.Parse(feedbackModel.Id),
                Description = feedbackModel.Description,
                UserId = feedbackModel.UserId,
                Name = feedbackModel.Name
            };
            await _feedbackService.Update(id, feedbackEdited).ConfigureAwait(false);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task Delete(string id)
        {
            await _feedbackService.Remove(id).ConfigureAwait(false);
        }
    }
}
