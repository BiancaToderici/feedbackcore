﻿using FeedbackCore.Common;
using FeedbackCore.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FeedbackCore.Services
{
     public interface IFeedbackService
    {
        Task<IEnumerable<FeedbackModel>> Get();
        Task<IEnumerable<Feedback>> GetByUserName(string userName);
        Task<Feedback> GetById(string id);
        Task Create(Feedback feedback);
        Task Update(string id, Feedback feedback);
        Task Remove(string id);
        Task<IEnumerable<string>> Search(string search);
    }
}