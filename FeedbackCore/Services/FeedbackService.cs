﻿using FeedbackCore.Common;
using FeedbackCore.Models;
using FeedbackCore.Repositories;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FeedbackCore.Services
{
    public class FeedbackService : IFeedbackService
    {
        private readonly IFeedbackRepository _feedbackRepository;
        public FeedbackService(IFeedbackRepository feedbackRepository)
        {
            _feedbackRepository = feedbackRepository;
        }
        public async Task Create(Feedback feedback)
        {
            await _feedbackRepository.Create(feedback).ConfigureAwait(false);
        }

        public async Task<IEnumerable<FeedbackModel>> Get()
        {
            var feedbacks = await _feedbackRepository.Get().ConfigureAwait(false);
            var list = new List<FeedbackModel>();
            foreach (var item in feedbacks)
            {
                FeedbackModel feedbackModel = new FeedbackModel
                {
                    Name = item.Name,
                    Description = item.Description,
                    DateTime = item.DateTime,
                    Id = item.Id.ToString(),
                    UserId = item.UserId
                };
                list.Add(feedbackModel);
            }
            return list;
        }

        public async Task<IEnumerable<Feedback>> GetByUserName(string userName)
        {
            var feedbacks = await _feedbackRepository.GetByUserName(userName).ConfigureAwait(false);
            return feedbacks;
        }

        public async Task<Feedback> GetById(string id)
        {
            var feedback = await _feedbackRepository.GetById(id).ConfigureAwait(false);
            return feedback;
        }

        public async Task Remove(string id)
        {
            await _feedbackRepository.Remove(id).ConfigureAwait(false);
        }

        public async Task Update(string id, Feedback feedback)
        {
            var feedbackExists = _feedbackRepository.GetById(id);
            if (feedbackExists != null)
            {
                await _feedbackRepository.Update(id, feedback).ConfigureAwait(false);
            }
        }

        public async Task<IEnumerable<string>> Search(string search)
        {
            var names = await _feedbackRepository.Search(search).ConfigureAwait(false);
            return names;
        }

    }
}

